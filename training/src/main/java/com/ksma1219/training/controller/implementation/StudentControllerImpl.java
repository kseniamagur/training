package com.ksma1219.training.controller.implementation;

import com.ksma1219.training.controller.interfaces.StudentController;
import com.ksma1219.training.entity.Student;
import com.ksma1219.training.exceptions.ConstraintException;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;
import com.ksma1219.training.service.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/student")
public class StudentControllerImpl implements StudentController {

    @Autowired
    private StudentService studentService;

    private static final String CREATE_STUDENT_ERR_MSG = "Unable to create student with group that doesn't exist";

    @Override
    public ResponseEntity<Student> createStudent(@RequestBody Student student) throws EmptyArgumentException, DbException {
        try {
            return ResponseEntity.ok(studentService.createStudent(student));
        } catch (ConstraintException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, CREATE_STUDENT_ERR_MSG);
        }
    }

    @Override
    public ResponseEntity<Student> getStudent(long id) throws DbException {
        Optional<Student> studentOptional = studentService.getStudent(id);
        return studentOptional.isPresent() ? ResponseEntity.ok(studentOptional.get()) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<List<Student>> getStudents() throws DbException {
        return ResponseEntity.ok(studentService.getStudents());
    }

    @Override
    public ResponseEntity<Student> updateStudent(@RequestBody Student student) throws EmptyArgumentException, DbException {
        Student studentUpdated = studentService.updateStudent(student);
        return ResponseEntity.ok(studentUpdated);
    }

    @Override
    public ResponseEntity<?> deleteStudent(long id) throws DbException {
        studentService.deleteStudent(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
