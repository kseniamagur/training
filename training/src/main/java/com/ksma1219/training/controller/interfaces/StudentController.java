package com.ksma1219.training.controller.interfaces;

import com.ksma1219.training.entity.Student;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


public interface StudentController {

    @PostMapping("/create")
    ResponseEntity<Student> createStudent(@RequestBody Student student) throws EmptyArgumentException, DbException;

    @GetMapping("")
    ResponseEntity<Student> getStudent(@RequestParam long id) throws DbException;

    @GetMapping("/all")
    ResponseEntity<List<Student>> getStudents() throws DbException;

    @PutMapping("/update")
    ResponseEntity<Student> updateStudent(@RequestBody Student student) throws EmptyArgumentException, DbException;

    @DeleteMapping("/delete/{id}")
    ResponseEntity<?> deleteStudent(@PathVariable("id") long id) throws DbException;
}
