package com.ksma1219.training.controller.interfaces;

import com.ksma1219.training.entity.Group;
import com.ksma1219.training.entity.Student;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface GroupController {
    @PostMapping("/create")
    ResponseEntity<Group> createGroup(@RequestBody Group group) throws DbException, EmptyArgumentException;

    @GetMapping("")
    ResponseEntity<Group> getGroup(@RequestParam long id) throws DbException;

    @GetMapping("/all")
    ResponseEntity<List<Group>> getGroups() throws DbException;

    @PutMapping("/update")
    ResponseEntity<Group> updateGroup(@RequestBody Group group) throws EmptyArgumentException, DbException;

    @DeleteMapping("/delete/{id}")
    ResponseEntity<?> deleteGroup(@PathVariable("id") long id) throws DbException;
}
