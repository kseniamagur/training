package com.ksma1219.training.controller.implementation;

import com.ksma1219.training.controller.interfaces.GroupController;
import com.ksma1219.training.entity.Group;
import com.ksma1219.training.exceptions.ConstraintException;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;
import com.ksma1219.training.service.interfaces.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/group")
public class GroupControllerImpl implements GroupController {

    @Autowired
    private GroupService groupService;

    private static final String DELETE_GROUP_ERR_MSG = "Unable to delete not empty group";

    @Override
    public ResponseEntity<Group> createGroup(Group group) throws DbException, EmptyArgumentException {
        return ResponseEntity.ok(groupService.createGroup(group));
    }

    @Override
    public ResponseEntity<Group> getGroup(long id) throws DbException {
        Optional<Group> groupOptional = groupService.getGroup(id);
        return groupOptional.isPresent() ? ResponseEntity.ok(groupOptional.get()) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<List<Group>> getGroups() throws DbException {
        return ResponseEntity.ok(groupService.getAllGroups());
    }

    @Override
    public ResponseEntity<Group> updateGroup(Group group) throws EmptyArgumentException, DbException {
        return ResponseEntity.ok(groupService.updateGroup(group));
    }

    @Override
    public ResponseEntity<?> deleteGroup(long id) throws DbException {
        try {
            groupService.deleteGroup(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ConstraintException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, DELETE_GROUP_ERR_MSG);
        }
    }
}
