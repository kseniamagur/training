package com.ksma1219.training.exceptions;

//thrown on database constraint violation
public class ConstraintException extends DbException {
    public ConstraintException(String msg) {
        super(msg);
    }
}
