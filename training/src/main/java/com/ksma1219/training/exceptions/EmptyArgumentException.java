package com.ksma1219.training.exceptions;

//thrown when argument is null
public class EmptyArgumentException extends Exception {
    public EmptyArgumentException(String msg){
        super(msg);
    }
}
