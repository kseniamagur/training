package com.ksma1219.training.exceptions.handler;

import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

//Handles exceptions thrown from controllers
@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String DB_ERR_MSG = "Database exception";
    private static final String EMPTY_ARG_ERR_MSG = "Argument cannot be empty";

    @ExceptionHandler(value = DbException.class)
    protected ResponseEntity<String> handleDbException(DbException e) {
        return new ResponseEntity<>(DB_ERR_MSG, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(value = EmptyArgumentException.class)
    protected ResponseEntity<String> handleEmptyArgumentException(EmptyArgumentException e) {
        return new ResponseEntity<>(EMPTY_ARG_ERR_MSG, HttpStatus.BAD_REQUEST);
    }
}
