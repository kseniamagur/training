package com.ksma1219.training.exceptions;

//general database exception
public class DbException extends Exception {
    public DbException(String msg){
        super(msg);
    }
}
