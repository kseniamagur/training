package com.ksma1219.training.service.interfaces;

import com.ksma1219.training.entity.Student;
import com.ksma1219.training.exceptions.ConstraintException;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    Student createStudent(Student student) throws DbException, EmptyArgumentException;

    Optional<Student> getStudent(long id) throws DbException;

    List<Student> getStudents() throws DbException;

    Student updateStudent(Student student) throws DbException, EmptyArgumentException;

    void deleteStudent(long id) throws DbException;

}
