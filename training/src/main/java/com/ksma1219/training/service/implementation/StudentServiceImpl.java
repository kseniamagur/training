package com.ksma1219.training.service.implementation;

import com.ksma1219.training.entity.Student;
import com.ksma1219.training.exceptions.ConstraintException;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;
import com.ksma1219.training.repository.interfaces.StudentRepository;
import com.ksma1219.training.service.interfaces.StudentService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student createStudent(Student student) throws ConstraintException, DbException, EmptyArgumentException {
        try {
            return studentRepository.save(student);
        } catch (DataIntegrityViolationException e) {
            throw new ConstraintException(e.toString());
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new EmptyArgumentException(e.getMessage());
        }
    }

    @Override
    public Optional<Student> getStudent(long id) throws DbException {
        try {
            return studentRepository.findById(id);
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        }
    }

    @Override
    public List<Student> getStudents() throws DbException {
        try {
            return studentRepository.findAll();
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        }
    }

    @Override
    public Student updateStudent(Student student) throws DbException, EmptyArgumentException {
        try {
            return studentRepository.save(student);
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new EmptyArgumentException(e.getMessage());
        }
    }

    @Override
    public void deleteStudent(long id) throws DbException {
        try {
            studentRepository.deleteById(id);
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        }
    }
}
