package com.ksma1219.training.service.implementation;

import com.ksma1219.training.entity.Group;
import com.ksma1219.training.exceptions.ConstraintException;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;
import com.ksma1219.training.repository.interfaces.GroupRepository;
import com.ksma1219.training.service.interfaces.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Override
    public Group createGroup(Group group) throws DbException, EmptyArgumentException {
        try {
            return groupRepository.save(group);
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new EmptyArgumentException(e.getMessage());
        }

    }

    @Override
    public Optional<Group> getGroup(long id) throws DbException {
        try {
            return groupRepository.findById(id);
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        }
    }

    @Override
    public List<Group> getAllGroups() throws DbException {
        try {
            return groupRepository.findAll();
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        }
    }

    @Override
    public Group updateGroup(Group group) throws DbException, EmptyArgumentException {
        try {
            return groupRepository.save(group);
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new EmptyArgumentException(e.getMessage());
        }
    }

    @Override
    public void deleteGroup(long id) throws DbException, ConstraintException {
        try {
            groupRepository.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            throw new ConstraintException(e.getMessage());
        } catch (DataAccessException e) {
            throw new DbException(e.getMessage());
        }
    }
}
