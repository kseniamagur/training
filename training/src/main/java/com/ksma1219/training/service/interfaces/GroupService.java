package com.ksma1219.training.service.interfaces;

import com.ksma1219.training.entity.Group;
import com.ksma1219.training.exceptions.ConstraintException;
import com.ksma1219.training.exceptions.DbException;
import com.ksma1219.training.exceptions.EmptyArgumentException;

import java.util.List;
import java.util.Optional;

public interface GroupService {
    Group createGroup(Group group) throws DbException, EmptyArgumentException;

    Optional<Group> getGroup(long id) throws DbException;

    List<Group> getAllGroups() throws DbException;

    Group updateGroup(Group group) throws DbException, EmptyArgumentException;

    void deleteGroup(long id) throws DbException, ConstraintException;
}
